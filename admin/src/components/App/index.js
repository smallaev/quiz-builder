import React from "react";
import {hot} from "react-hot-loader";
import {
	BrowserRouter as Router,
	Switch,
	Route
} from "react-router-dom";
import { createGlobalStyle } from 'styled-components';
import Page from 'components/page/Page';
import EditFormContainer from 'components/forms/EditForm/EditFormContainer';

const GlobalStyle = createGlobalStyle`
	html,
	body {
		padding: 0;
		margin: 0;
		font-family: 'Barlow Semi Condensed', sans-serif;
	}
`;

const App = () => {
	return (<>
		<GlobalStyle />
		<Page>
			<Router>
				<Switch>
					<Route path="/questions/:questionID/edit/" exact={true}>
						<EditFormContainer />
					</Route>
					<Route path="/questions/:questionID/edit/options/add">
						<EditFormContainer action="new-option" />
					</Route>
					<Route path="/questions/new/">
						<EditFormContainer />
					</Route>
				</Switch>
			</Router>
		</Page>
	</>);
}

export default hot(module)(App);