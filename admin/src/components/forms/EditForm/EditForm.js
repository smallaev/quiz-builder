import React, { useState, useEffect, useContext } from "react";
import { useParams, useHistory } from "react-router-dom";
import styled from "styled-components";
import axios from "axios";
import { EditFormContext } from "context/EditFormContext";
import { apiUrl, assetsUrl } from "components/utils/getUrl";
import Loading from "components/forms/Loading/Loading";
import TextField from "components/forms/TextField/TextField";

const FormLayout = styled.div`
	max-width: 800px;
	margin: 0 auto;
`;

const QuestionContainer = styled.div`
	display: flex;
	flex-direction: column;
`;

const OptionsContainer = styled.div`
	display: flex;
	/* flex-direction: column; */
`;

const NewOptionButton = styled.button`
	
`;

function EditForm(props) {

	const params = useParams();
	const history = useHistory();
	const editFormContext = useContext(EditFormContext);

	const [questionData, setQuestionData] = useState(null);
	
	useEffect(() => {
		console.log(props.action);
		if (params.questionID) {			
			(async () => {
				const url = `${apiUrl}/questions/${params.questionID}`;
				try {
					const result = await axios.get(url);
					const {data} = result;
					if (data && data.questions) {
						setQuestionData(data.questions);
					} else {
						// error
					}
					editFormContext.setIsLoading(false);
				} catch (e) {
					
				}
			})();
		} else {
			editFormContext.setIsLoading(false);
		}

	}, [params.questionID, props.action]);

	const onFormSubmit = (e) => {
		e.preventDefault();
		(async () => {
			const data = {
				questionText,
				answerText
			};
			const url = `${apiUrl}/questions/${params.questionID}/edit`;
			try {
				const result = await axios.post(url, data);
			} catch (e) {
				
			}
		})();
	}

	const enableNewOptionField = () => {
		history.push(`/questions/${params.questionID}/edit/options/add`);
		// setShowNewOptionField(true);
	}

	const onQuestionTextChange = (e) => {
		setQuestionText(e.target.value);
	}

	const getQuestionFields = () => {
		const fields = [];
		if (questionData) {
			for (const questionID in questionData) {
				if (questionData.hasOwnProperty(questionID)) {
					const question = questionData[questionID];
					if (question && question.questionText) {
						fields.push(<TextField action="edit-question" questionID={questionID} id={'qs'+questionID} value={question.questionText} onChange={onQuestionTextChange} />);
					}
				}
			}
		} else {
			fields.push(<TextField action="new-question" id={'new'} />);
		}
		
		return fields;
	}

	const isNewOptionMode = () => {
		return props.action === 'new-option';
	}

	const getOptionsFields = () => {
		const fields = [];

		if (questionData) {
			for (const questionID in questionData) {
				if (questionData.hasOwnProperty(questionID)) {
					const question = questionData[questionID];
					if (question && question.options) {
						question.options.forEach(option => {
							fields.push(<TextField action="edit-option" optionID={option.optionID} isCorrect={option.isCorrect} id={'op'+option.optionID} value={option.optionText} onChange={onQuestionTextChange} />);
						});
					}

					if (isNewOptionMode()) {
						fields.push(<TextField action="new-option" isCorrect={false} id={'new'} questionID={questionID} onChange={onQuestionTextChange} />);
					}
				}
			}
		} else {
			// fields.push(<TextField action="new-option" id={'new'} />);
		}
		
		return fields;
	}

	return (
		<Loading>
			<FormLayout>
				<form onSubmit={onFormSubmit}>
					<QuestionContainer>
						{getQuestionFields()}
					</QuestionContainer>
					<OptionsContainer>
						{getOptionsFields()}
					</OptionsContainer>
					{isNewOptionMode() ? null : <NewOptionButton onClick={enableNewOptionField}>Add option</NewOptionButton>}
					{/* <SubmitButton /> */}
				</form>
			</FormLayout>
		</Loading>
	);
}

export default EditForm;