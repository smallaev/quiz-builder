import React from "react";
import EditForm from "components/forms/EditForm/EditForm";
import { EditFormProvider } from "context/EditFormContext";

const EditFormContainer = (props) => {
	return (
		<EditFormProvider>
			<EditForm action={props.action}>
				{props.children}
			</EditForm>
		</EditFormProvider>
	);
}

export default EditFormContainer;