import React, { useContext } from "react";
import { EditFormContext } from "context/EditFormContext";

const Loading = (props) => {
	const editFormContext = useContext(EditFormContext);
	
	let ret;
	if (!editFormContext.isLoading) {
		ret = props.children;
	} else {
		ret = 'Loading ...';
	}
	return ret;
};

export default Loading;