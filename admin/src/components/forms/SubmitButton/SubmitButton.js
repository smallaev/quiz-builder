import React from "react";
import styled from "styled-components";

const SubmitButtonStyled = styled.input`

`;

const SubmitButton = (props) => {
	return (
		<SubmitButtonStyled type="submit" />
	);
}

export default SubmitButton;