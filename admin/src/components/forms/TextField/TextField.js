import React, { useEffect, useState } from "react";
import styled from "styled-components";
import { useHistory, useParams } from "react-router-dom";

import axios from "axios";
import { apiUrl, assetsUrl } from "components/utils/getUrl";

const TextFieldStyled = styled.textarea`

`;

function TextField(props) {

	const [value, setValue] = useState(props.value);
	const [dbValue, setDbValue] = useState(props.value);
	const params = useParams();
	const history = useHistory();

	const onChange = (e) => {
		setValue(e.target.value);
	}

	let onSubmit = (e) => {
		(async () => {
			if (dbValue !== e.target.value) {
				let url;
				let data;
				if (props.action === "new-question") {
					url = `${apiUrl}/questions/new`;
					data = {
						questionText: e.target.value
					}
				} else if (props.action === "edit-question") {
					url = `${apiUrl}/questions/${props.questionID}/edit`;
					data = {
						questionText: e.target.value
					}
				} else if (props.action === "edit-option") {
					url = `${apiUrl}/options/${props.optionID}/edit`;
					data = {
						optionText: e.target.value
					}
				} else if (props.action === "new-option") {
					url = `${apiUrl}/questions/${props.questionID}/options/new`;
					data = {
						optionText: e.target.value,
						correct: true
					}
				}

				try {
					const result = await axios.post(url, data);
					if (props.action === 'new-question') {
						if (result && result.data && result.data.result.insertId) {
							history.push(`/questions/${result.data.result.insertId}/edit/`);
						}
					}
					if (props.action === 'new-option') {
						if (result && result.data && result.data.result.insertId) {
							history.push(`/questions/${params.questionID}/edit/`);
						}
					}
				} catch (e) {
					
				}
			}
		})();
	}

	return (
		<TextFieldStyled key={props.id} value={value} onChange={onChange} onBlur={onSubmit} />
	);
}

export default TextField;