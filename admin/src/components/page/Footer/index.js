import React from "react";
import styled from "styled-components";

const FooterStyled = styled.div`
    padding: 1em;
`;

const Footer = () => {
	return (
		<FooterStyled>
            Footer
		</FooterStyled>
	);
}

export default Footer;