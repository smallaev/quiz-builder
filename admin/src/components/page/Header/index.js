import React from "react";
import styled from "styled-components";

const HeaderStyled = styled.div`
    padding: 1em;
`;

const Header = () => {
	return (
		<HeaderStyled>
            Header
		</HeaderStyled>
	);
}

export default Header;