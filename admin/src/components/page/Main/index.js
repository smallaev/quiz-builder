import React from "react";
import styled from "styled-components";

const MainStyled = styled.main`
    flex: 1;
`;

const Main = (props) => {
	return (
		<MainStyled>
			{props.children}
		</MainStyled>
	);
}

export default Main;