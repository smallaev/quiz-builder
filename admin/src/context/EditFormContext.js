import React, { useState, createContext } from "react";

export const EditFormContext = createContext();

export const EditFormProvider = (props) => {
	const [questions, setQuestions] = useState([]);
	const [isAnswerGiven, setIsAnswerGiven] = useState(false);
	const [currentQuestionIdx, setCurrentQuestionIdx] = useState(0);
	const [currentQuestion, setCurrentQuestion] = useState({});
	const [isLoading, setIsLoading] = useState(true);
	
	return (
		<EditFormContext.Provider value={{
			questions,
			setQuestions,
			isAnswerGiven,
			setIsAnswerGiven,
			currentQuestionIdx,
			setCurrentQuestionIdx,
			currentQuestion,
			setCurrentQuestion,
			isLoading,
			setIsLoading,
		}}>
			{props.children}
		</EditFormContext.Provider>
	)
}
