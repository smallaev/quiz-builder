import React from 'react';
import {hot} from 'react-hot-loader';
import {
	BrowserRouter as Router,
	Switch,
	Route,
	Link
} from 'react-router-dom';
import { createGlobalStyle } from 'styled-components';
import Page from 'components/page/Page';
import QuizContainer from 'components/game/Quiz/QuizContainer';


const GlobalStyle = createGlobalStyle`
	html,
	body {
		padding: 0;
		margin: 0;
		font-family: 'Barlow Semi Condensed', sans-serif;
	}
`;

const App = () => {
	return (<>
		<GlobalStyle />
		<Page>
			<Router>
				<Switch>
					<Route path="/quiz/:topic">
						<QuizContainer />
					</Route>
				</Switch>
			</Router>
		</Page>
	</>);
}

export default hot(module)(App);