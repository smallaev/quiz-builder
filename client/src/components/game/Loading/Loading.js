import React, { useContext } from 'react';
import { QuizContext } from 'context/QuizContext';

const Loading = (props) => {
	let quizContext = useContext(QuizContext);
	
	let ret;
	if (quizContext.questions.length) {
		ret = props.children;
	} else {
		ret = 'Loading ...';
	}
	return ret;
};

export default Loading;