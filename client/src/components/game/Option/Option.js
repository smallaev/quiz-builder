import React, { useContext } from 'react';
import axios from 'axios';
import { QuizContext } from 'context/QuizContext';
import { OptionStyled } from './Styled';

let Option = (props) => {
	const { 
		setIsAnswerGiven,
		isAnswerGiven,
	} = useContext(QuizContext);

	const getCorrectAnswer = async (questionID) => {
		const url = 'http://localhost:3010/question/correct-answer';
		const result = await axios.post(url, {
			questionID: questionID
		}, {
			headers: {
				'Content-Type': 'application/json'
			}
		});
		
		return result.data.result;
	};
	
	const handleOptionClick = async (questionID, optionID) => {
		if (!isAnswerGiven) {
			const answerID = await getCorrectAnswer(questionID);
			const isCorrect = answerID === optionID;
			const statuses = {};
			if (isCorrect) {
				statuses[optionID] = 'correct';
			} else {
				statuses[optionID] = 'incorrect';
				statuses[answerID] = 'correct';
			}
			props.updateOptionStatuses(statuses);
			setIsAnswerGiven(true);
		}
	};

	return <OptionStyled 
			status={props.status} 
			onClick={() => handleOptionClick(props.questionID, props.optionID)}
		>
			{props.children}
		</OptionStyled>;
};

export default Option;