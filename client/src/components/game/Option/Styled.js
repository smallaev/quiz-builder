import styled from 'styled-components';

export const OptionStyled = styled.div`
	padding: 1.5em;
	border: 1px solid #292929;
	border-radius: 15px;
	box-sizing: border-box;
	margin: 1em 0;
	width: 48%;
	cursor: pointer;

	${props => props.status === 'correct' && `
		background: green;
	`}
	${props => props.status === 'incorrect' && `
		background: red;
	`}
`;