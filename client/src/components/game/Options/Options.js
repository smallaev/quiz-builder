import React, { useEffect, useState, useContext } from 'react';
import Option from '../Option/Option';
import { QuizContext } from 'context/QuizContext';
import { OptionsStyled } from './Styled';

const Options = (props) => {
	const [optionsStatuses, setOptionsStatuses] = useState({});
	const { 
		isAnswerGiven,
		currentQuestion,
	} = useContext(QuizContext);

	useEffect(() => {
		if (!isAnswerGiven) {
			updateOptionStatuses();
		}
	}, [isAnswerGiven]);

	const updateOptionStatuses = (statuses = null) => {
		const tempStatuses = {};
		if (currentQuestion && currentQuestion.options) {

			for (const option of currentQuestion.options) {
				tempStatuses[option.id] = 'none';
			}
			if (statuses) {
				for (let optionID in statuses) {					
					tempStatuses[optionID] = statuses[optionID];
				}
			}
		}
		setOptionsStatuses(tempStatuses);
	};


	const ret = [];
	if (currentQuestion && currentQuestion.options) {
		let key = 0;
		for (const option of currentQuestion.options) {
			ret.push(
				<Option 
					questionID={currentQuestion.id} 
					optionID={option.id} 
					status={optionsStatuses[option.id]} 
					updateOptionStatuses={updateOptionStatuses}
					key={++key}
				>
					{option.optionHtml}
				</Option>
			);
		}
		return <OptionsStyled>
			{ret}
		</OptionsStyled>;
	}
	return '';
};

export default Options;