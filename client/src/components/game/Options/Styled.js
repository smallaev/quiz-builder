import styled from 'styled-components';

export const OptionsStyled = styled.div`
	display: flex;
	flex-wrap: wrap;
	justify-content: space-between;

	font-family: 'Barlow Semi Condensed', sans-serif;

`;