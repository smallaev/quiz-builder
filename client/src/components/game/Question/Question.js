import React, { useEffect, useContext } from 'react';
import Options from '../Options/Options';
import { QuizContext } from 'context/QuizContext';
import ReactHtmlParser from 'react-html-parser';
import { apiUrl, assetsUrl } from 'components/utils/getUrl';

import {
	QuestionContainerStyled, 
	QuestionControlsStyled,
	QuestionElementStyled,
	QuestionStyled
} from "./Styled";

const parseHtml = (html) => {
	// replacing assets url placehlder with actual assets url
	const readyHtml = html.replace(/\[ASSETS\]/g, assetsUrl);
	const htmlParsed = ReactHtmlParser(readyHtml);
	return htmlParsed;
};

const QuestionHtml = (props) => {

	if (!(props.question && props.question.questionHtml)) {
		return '';
	}

	const questionHtml = props.question.questionHtml;
	
	const ret = [];
	if (Array.isArray(questionHtml)) {
		questionHtml.forEach(html => {
			const htmlParsed = parseHtml(html);
			ret.push(<QuestionElementStyled>{htmlParsed}</QuestionElementStyled>);
		});
	} else {
		const htmlParsed = parseHtml(questionHtml);
		ret.push(<QuestionElementStyled>{htmlParsed}</QuestionElementStyled>);
	}

	return <QuestionStyled>
		{ret}
	</QuestionStyled>;
};

const Question = (props) => {
	const { 
		questions, 
		currentQuestionIdx, 
		setCurrentQuestionIdx, 
		isAnswerGiven,
		setIsAnswerGiven,
		currentQuestion,
		setCurrentQuestion,
	} = useContext(QuizContext);
	
	const nextQuestion = () => {
		setCurrentQuestionIdx(currentQuestionIdx => currentQuestionIdx + 1);
		setIsAnswerGiven(false);
	};

	useEffect(() => {
		setCurrentQuestion(questions[currentQuestionIdx]);
	}, [currentQuestionIdx]);

	return (<>
		<QuestionContainerStyled>
			<QuestionHtml question={currentQuestion} />
			<Options />
		</QuestionContainerStyled>
		<QuestionControlsStyled>
			<button onClick={nextQuestion}>Next</button>
		</QuestionControlsStyled>
	</>);
}

export default Question;