import styled from 'styled-components';

export const QuestionElementStyled = styled.div`
	margin: .5em 0;
`;

export const QuestionStyled = styled.div`
	display: flex;
	flex-direction: column;
	align-items: center;
	margin: .5em 0;
`;

export const QuestionContainerStyled = styled.div`
	border-radius: 5px;
`;

export const QuestionControlsStyled = styled.div`
	padding: 1em;
	border: 1px solid #eee;
	border-radius: 5px;
`;