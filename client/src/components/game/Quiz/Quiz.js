import React, { useState, useEffect, useContext } from 'react';
import styled from 'styled-components';
import { useParams } from 'react-router-dom';
import axios from 'axios';
import Question from 'components/game/Question/Question';
import Loading from 'components/game/Loading/Loading';
import { QuizContext } from 'context/QuizContext';
import { apiUrl, assetsUrl } from 'components/utils/getUrl';

const QuizLayout = styled.div`
	max-width: 800px;
	margin: 0 auto;
`;

const Quiz = (props) => {
	const params = useParams();
	const quizContext = useContext(QuizContext);
	
	useEffect(() => {
		(async () => {
			const url = apiUrl + '/quiz/' + params.topic;
			const result = await axios.get(url);
			console.log(result);
			quizContext.setQuestions(result.data.questions);
		})();
	}, []);	

	return (
		<QuizLayout>
			<Loading>
				<Question />
			</Loading>
		</QuizLayout>
	);
}

export default Quiz;