import React from "react";
import Quiz from "components/game/Quiz/Quiz";
import { QuizProvider } from "context/QuizContext";

const QuizContainer = (props) => {
	return (
		<QuizProvider>
			<Quiz>
				{props.children}
			</Quiz>
		</QuizProvider>
	);
}

export default QuizContainer;