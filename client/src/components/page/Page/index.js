import React from 'react';
import styled from 'styled-components';
import Header from 'components/page/Header';
import Footer from 'components/page/Footer';
import Main from 'components/page/Main';

const PageLayout = styled.div`
    display: flex;
    min-height: 100vh;
    flex-direction: column;
`;

const Page = (props) => {
	return (
		<PageLayout>
            <Header />
            <Main>
                {props.children}
            </Main>
            <Footer />
		</PageLayout>
	);
}

export default Page;