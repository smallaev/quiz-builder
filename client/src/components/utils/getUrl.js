const MODE = process.env.MODE || 'dev';
const apiUrl = (MODE === 'prod' && process.env.API_URL_PROD) ? process.env.API_URL_PROD : process.env.API_URL_DEV;
const assetsUrl = (MODE === 'prod' && process.env.ASSETS_URL_PROD) ? process.env.ASSETS_URL_PROD : process.env.ASSETS_URL_DEV;

console.log(process.env);
module.exports = {
	apiUrl,
	assetsUrl
};
