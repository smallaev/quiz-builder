import React, { useState, createContext } from "react";

export const QuizContext = createContext();

export const QuizProvider = (props) => {
	const [questions, setQuestions] = useState([]);
	const [isAnswerGiven, setIsAnswerGiven] = useState(false);
	const [currentQuestionIdx, setCurrentQuestionIdx] = useState(0);
	const [currentQuestion, setCurrentQuestion] = useState({});
	
	return (
		<QuizContext.Provider value={{
			questions,
			setQuestions,
			isAnswerGiven,
			setIsAnswerGiven,
			currentQuestionIdx,
			setCurrentQuestionIdx,
			currentQuestion,
			setCurrentQuestion,
		}}>
			{props.children}
		</QuizContext.Provider>
	)
}
