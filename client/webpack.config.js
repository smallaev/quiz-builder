const path = require("path");
const webpack = require("webpack");
module.exports = {
	entry: "./src/index.js",
	mode: "development",
	module: {
		rules: [
			{
				test: /\.(js|jsx)$/,
				exclude: /(node_modules|bower_components)/,
				loader: "babel-loader",
				options: {
					presets: [
						"@babel/env"
					]
				}
			},
			{
				test: /\.css$/,
				use: [
					"style-loader", 
					"css-loader"
				]
			}
		]
	},
	resolve: { 
		modules: [path.resolve(__dirname, './src'), path.resolve(__dirname, './node_modules')],
		extensions: ["*", ".js", ".jsx"] 
	},
	output: {
		path: path.resolve(__dirname, "dist/"),
		publicPath: "/dist/",
		filename: "bundle.js"
	},
	devServer: {
		historyApiFallback: true,
		contentBase: path.join(__dirname, "public/"),
		host: '0.0.0.0',
		port: 3000,
		publicPath: "http://localhost:3000/dist/",
		hotOnly: true
	},
	plugins: [
		new webpack.HotModuleReplacementPlugin(),
		new webpack.DefinePlugin({
			'process.env': {
				API_URL_DEV: JSON.stringify(process.env.API_URL_DEV),
				API_URL_PROD: JSON.stringify(process.env.API_URL_PROD),
				ASSETS_URL_DEV: JSON.stringify(process.env.ASSETS_URL_DEV),
				ASSETS_URL_PROD: JSON.stringify(process.env.ASSETS_URL_PROD),
				MODE: JSON.stringify(process.env.MODE)
			},
		  }),
	]
};