const pool = require("../../db/connect");

module.exports = async (req, res, next) => {
	let result;
	console.log(req.body.optionText);
	console.log(req.params.optionID);
	try {
		const [optionRes] = await pool.query(`
			UPDATE 
				qz_option
			SET
				option_text = '${req.body.optionText}'
			WHERE
				id = ${req.params.optionID}
		`);
		result = optionRes;
		
	} catch (error) {
		console.error('Unable to connect to the database:', error);
	}

	res.json({
		options: result
	});
};