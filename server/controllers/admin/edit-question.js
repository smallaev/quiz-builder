const pool = require("../../db/connect");

module.exports = async (req, res, next) => {
	console.log(req.body);
	console.log(req.params);

	let result;
	try {
		const [questionRes] = await pool.query(`
			UPDATE 
				question
			SET
				question_text = '${req.body.questionText}'
			WHERE
				id = ${req.params.questionID}
		`);
		result = questionRes;
		
	} catch (error) {
		console.error('Unable to connect to the database:', error);
	}

	res.json({
		questions: result
	});
};