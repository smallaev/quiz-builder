const pool = require("../../db/connect");

module.exports = async (req, res, next) => {
	let questions;
	try {
		const [rows,fields] = await pool.query(`
			SELECT 
				question.id as question_id,
				question_options.option_id as option_id,
				question.question_text AS question_text,
				qz_option.option_text AS option_text,
				question_options.correct AS correct
			FROM 
				question
			LEFT JOIN
				question_options
				ON question_options.question_id = question.id
			LEFT JOIN
				qz_option
				ON question_options.option_id = qz_option.id
			WHERE
				question.id = ${req.params.questionID}
		`);

		console.log(rows);

		if (rows) {
			questions = {};
			rows.forEach(row => {
				if (!questions[row.question_id]) {
					questions[row.question_id] = {};
					questions[row.question_id].questionText = row.question_text;
					questions[row.question_id].options = [];
				}

				if (row.option_id) {
					questions[row.question_id].options.push({
						optionID: row.option_id,
						optionText: row.option_text,
						isCorrect: row.correct
					});
				}
			});
		} else {
			throw new Error(`Not found`);
		}
	} catch (error) {
		console.error('Unable to connect to the database:', error);
	}

	res.json({
		questions
	});
};