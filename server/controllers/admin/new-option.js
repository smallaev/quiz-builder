const pool = require("../../db/connect");

module.exports = async (req, res, next) => {
	console.log(req.body);
	console.log(req.params);
	let result;
	try {
		const [res] = await pool.query(`
			INSERT INTO 
				qz_option (option_text)
			VALUES
				('${req.body.optionText}')
		`);
		if (res && res.insertId) {
			const [res1] = await pool.query(`
				INSERT INTO 
					question_options (question_id, option_id, correct)
				VALUES
					(${req.params.questionID}, ${res.insertId}, ${req.body.correct})
			`);
		}
		result = res;
		console.log(res);
	} catch (error) {
		console.error('Unable to connect to the database:', error);
	}

	res.json({
		result: result
	});
};