const pool = require("../../db/connect");

module.exports = async (req, res, next) => {
	console.log(req.body);
	console.log(req.params);
	let result;
	try {
		const [questionRes] = await pool.query(`
			INSERT INTO 
				question (question_text)
			VALUES
				('${req.body.questionText}')
		`);
		result = questionRes;
		
	} catch (error) {
		console.error('Unable to connect to the database:', error);
	}

	res.json({
		result: result
	});
};