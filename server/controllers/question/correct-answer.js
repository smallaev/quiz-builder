const pool = require("../../db/connect");
let data = require("../../questions");

module.exports = async (req, res, next) => {

	// try {
	// 	const [rows,fields] = await pool.query("SELECT * FROM question");
	// } catch (error) {
	// 	console.error('Unable to connect to the database:', error);
	// }

	res.json({
		result: data.answers[req.body.questionID]
	});
};