const pool = require("../../db/connect");

module.exports = async (req, res, next) => {
	let questions;
	try {
		const [rows, fields] = await pool.query(`
			SELECT
				question.id AS question_id, 
				question.question_text AS question_text,
				qz_option.id AS option_id,
				qz_option.option_text AS option_text,
				question_options.correct AS option_correct
			FROM 
				question
			INNER JOIN
				question_options
				ON question_options.question_id = question.id
			INNER JOIN
				qz_option
				ON question_options.option_id = qz_option.id

		`);

		if (rows) {
			questions = [];

			rows.forEach(row => {
				if (!questions[row.question_id]) {
					questions[row.question_id] = {};
					questions[row.question_id].id = row.question_id;
					questions[row.question_id].questionHtml = row.question_text;
					questions[row.question_id].options = [];
				}
				if (row.option_id) {
					questions[row.question_id].options.push({
						id: row.option_id,
						optionHtml: row.option_text,
						isCorrect: row.correct
					});
				}
			});

			questions = questions.filter(_ => true);

		} else {
			throw new Error(`Not found`);
		}

	} catch (error) {
		console.error('Unable to connect to the database:', error);
	}

	res.json({
		questions: questions,
	});
};