let data = {
	questions: [
		{
			id: 1,
			questionHtml: [
				'text html hjiyu oukhd yiu yhigyukh ouihj', 
				'piuoigi pyy8igf',  
				'text joihghj',
			],
			options: [
				{
					id: 1,
					optionHtml: 'option 1',
				},
				{
					id: 2,
					optionHtml: 'option 2',
				}
			]
		},
		{
			id: 2,
			questionHtml: [
				'text here',
				'text here too',
				'<img width="200" src="[ASSETS]/images/flags/ar-flag.gif">',
			],
			options: [
				{
					id: 1,
					optionHtml: 'option 2-1',
				},
				{
					id: 2,
					optionHtml: 'option 2-2',
				},
				{
					id: 3,
					optionHtml: 'option 2-3',
				},
			]
		},
		{
			id: 3,
			questionHtml: 'text html 3',
			options: [
				{
					id: 1,
					optionHtml: 'option 3-1',
				},
				{
					id: 2,
					optionHtml: 'option 3-2',
				}
			]
		},
		{
			id: 4,
			questionHtml: 'text html 4',
			options: [
				{
					id: 1,
					optionHtml: 'option 4-1',
				},
				{
					id: 2,
					optionHtml: 'option 4-2',
				}
			]
		}
	],
	answers: {
		1: 2,
		2: 3,
		3: 1,
		4: 1,
	}
};

module.exports = data;