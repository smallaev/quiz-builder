-- UP_BEGIN

CREATE TABLE IF NOT EXISTS qz_option (
    id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    option_text TEXT
);

CREATE TABLE IF NOT EXISTS question_options (
    id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    question_id INT NOT NULL,
    option_id INT NOT NULL,
    FOREIGN KEY (question_id) REFERENCES question(id),
    FOREIGN KEY (option_id) REFERENCES qz_option(id)
);

CREATE TABLE IF NOT EXISTS question (
    id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    question_text TEXT,
    answer_id INT NOT NULL,
    FOREIGN KEY (answer_id) REFERENCES qz_option(id)  
);

-- UP_END

------------------

-- DOWN_BEGIN


-- DOWN_END