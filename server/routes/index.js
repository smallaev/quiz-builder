const express = require('express');
const router = express.Router();

const action = path => require(`../controllers/${path}`);

router.get('/', action('default/index'));
router.get('/users', action('users/index'));
router.get('/topic/:topic', action('topics/list-topics'));
router.get('/quiz/:topic', action('quiz/play'));
router.post('/question/correct-answer', action('question/correct-answer'));

router.get('/questions/:questionID', action('admin/get-question-data'));
router.post('/questions/:questionID/edit', action('admin/edit-question'));
router.post('/questions/new', action('admin/new-question'));
router.post('/options/:optionID/edit', action('admin/edit-option'));
router.post('/questions/:questionID/options/new', action('admin/new-option'));


module.exports = router;