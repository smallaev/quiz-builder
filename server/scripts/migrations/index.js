/**
 * Node.js script that manages SQL migration files
 * 
 */
const dotenv = require('dotenv');
const fs = require('fs');
const fsp = fs.promises;
const path = require('path');
const { exec } = require('child_process');
const mysql = require('mysql2');
dotenv.config();

let pool, conn;

const getPool = () => {
    pool = mysql.createPool({
        connectionLimit : 10,
        host : process.env.MYSQL_HOST,
        user : process.env.MYSQL_USER,
        password : process.env.MYSQL_PW,
        database : process.env.MYSQL_DB
    });

    pool = pool.promise();
    return pool;
}

const [nodePath, scriptPath, ...args] = process.argv;
const [command, migrationName] = args;

if (!process.env.MIGRATIONS_FOLDER) {
    console.log(`Please specify the migrations folder in the environment settings`);
    process.exit();
}

if (!command || !['migrate', 'undo', 'create'].includes(command)) {
    console.log(`${command} is an invalid command`);
    process.exit();
}

if (command === 'create') {
    migrationName = new Date().toISOString()
    .slice(0, 19)
    .replace('T', '_')
    .replace(':', '-')
    .replace(':', '-')
    .replace(':', '-')
    + (migrationName ? '_' + migrationName : '');
    
    const fileName = migrationName + '.sql';
    const filePath = path.join(process.env.MIGRATIONS_FOLDER, fileName);

    (async () => {
        try {
            const migrationTemplate = await fsp.readFile(path.join(__dirname, 'template.sql'));
            await fsp.writeFile(filePath, migrationTemplate);
        } catch (e) {
            console.log(e);
        }

    })();
}

const execSql = async (sql) => {
    try {
        await getPool();
        conn = await pool.getConnection();
        await conn.query('SET FOREIGN_KEY_CHECKS = 0;');
        await conn.query('START TRANSACTION');
        const queries = sql.trim().split(';');
        for (const query of queries) {

            console.log('=== query ===');
            console.log(query);
            console.log('=========');
            if (!query.replace(/\s/g, '').length) {
                console.log('string only contains whitespace (ie. spaces, tabs or line breaks)');
                continue;
            }
            await conn.query(query);
        }
    } catch (e) {
        await conn.query('ROLLBACK');
        console.log(e);
        process.exit();
    } finally {
        await conn.query('COMMIT');
        await conn.query('SET FOREIGN_KEY_CHECKS = 1;');
        conn.release();
    }
}

const  extractSql = async (migrationFileContent, mode = 'up') => {
    const sqlBegin, sqlEnd;
    if (mode === 'up') {
        sqlBegin = '-- UP_BEGIN';
        sqlEnd = '-- UP_END';
    } else if (mode === 'down') {
        sqlBegin = '-- DOWN_BEGIN';
        sqlEnd = '-- DOWN_END';
    } else {
        // error
    }

    const sqlCode = migrationFileContent.substring(
        migrationFileContent.lastIndexOf(sqlBegin) + sqlBegin.length,
        migrationFileContent.lastIndexOf(sqlEnd)
    );

    await execSql(sqlCode);    
}


const runMigrations = async (migrationsFolder) => {
    const files = await fsp.readdir(migrationsFolder);
    files.sort();
    for (const i = 0; i < files.length; i++) {
        const file = files[i];
        const fullPath = path.join(migrationsFolder, file);
        const migrationFile = await fsp.readFile(fullPath, 'utf8');
        await extractSql(migrationFile);

    }
};

if (command === 'migrate') {
    runMigrations(process.env.MIGRATIONS_FOLDER);
}









